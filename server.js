const http = require('http');
const express = require('express');
const app = express();

//Settings
app.set('port', process.env.PORT || 3001);
// const server2 = require('./routes/cuentasRouter')
const carsRoute = require('./routes/automovilesRoutes')

//Middlewares
app.use(express.json());

//Routes
// app.use('/api/server2', server2);
app.use('/api/cars', carsRoute);


// Starting the Server
app.listen(app.get('port'), () => {
    console.log('Server on port 3001');
});