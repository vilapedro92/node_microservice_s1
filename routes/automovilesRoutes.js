const express = require('express');
const router = express.Router();
const automovilesController = require('../controllers/automovilController');

router.get('/', automovilesController.getCars);

module.exports = router;