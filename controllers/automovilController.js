const http = require('http');

const automoviles = require('../models/automoviles');

const automovilesCtrl = {};
const autosPrecio = {}

//GET

automovilesCtrl.getCars = async (req, res) => {
    try{

        const options = {
            hostname: 'localhost',
            port: 3000,
            path: '/api/precio',
            method: 'GET'
          }
          //tambien es posible ponerle la ruta directamente al verbo

        await http.get(options, (resp) =>{
            const { statusCode } = resp;
            if(statusCode == 200){
                let pricesString = '';
                
                resp.on('data', (chunk) => {
                    pricesString += chunk;                    
                });

                resp.on('end', () => {
                    const priceArray = JSON.parse(pricesString);
                    console.log('Price', priceArray);
                    autosPrecio.autos =  createResponse(priceArray);                    
                });

                res.status(200).json(autosPrecio);   
                
            }            
        });     
       
    }catch{
        res.status(500).json({message: 'Error in server'});
    }
}

function createResponse(priceArray){
    return automoviles.map((auto) => {

        for(let indx = 0; indx < priceArray.length; indx++){
            const autoPrice = priceArray[indx];

            if( autoPrice.model === auto.model ){
                auto.price = autoPrice.price;
                return auto;
            }

        }

    })
}


module.exports = automovilesCtrl;


